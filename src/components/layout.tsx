import React from "react";
import Footer from "./footer";
import Nav from "./nav";

const Layout = ({ children }: { children: JSX.Element }) => {
  return (
    <div className="flex flex-col min-h-screen max-w-7xl m-auto px-4 py-4">
      <Nav />
      {children}
      <Footer />
    </div>
  );
};

export default Layout;

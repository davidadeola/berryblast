import React from "react";
import { CartIcon, PersonIcon } from "./icons";

const Nav = () => {
  return (
    <nav className="flex justify-between items-center text-white font-semiBold ">
      <h1 className="text-2xl font-bold">Berryblast</h1>
      <div className="flex gap-6 items-center">
        <div className="cursor-pointer w-12 hover:bg-green-500 rounded-full p-4 ease-in-out duration-300">
          <PersonIcon />
        </div>
        <div className="cursor-pointer w-14 hover:bg-green-500 rounded-full p-4 ease-in-out duration-300">
          <CartIcon />
        </div>
      </div>
    </nav>
  );
};

export default Nav;

## Berryblast-App

## Installations

firebase tools:

```
npm install firebase-tools -g
```

firebase functions:

```
npm install firebase-functions
```

firebase login (login CLI):

```
 firebase login
```

firebase function initialization:

```
firebase init
```
